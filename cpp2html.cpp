/*
 * CSc103 Project 5: Syntax highlighting, part two.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: Satya Gupta
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 7
 */

#include "fsm.h"
using namespace cppfsm;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <map>
using std::map;
#include <initializer_list> // for setting up maps without constructors.
#include <vector>
using std::vector;

// enumeration for our highlighting tags:
enum {
	hlstatement,  // used for "if,else,for,while" etc...
	hlcomment,    // for comments
	hlstrlit,     // for string literals
	hlpreproc,    // for preprocessor directives (e.g., #include)
	hltype,       // for datatypes and similar (e.g. int, char, double)
	hlnumeric,    // for numeric literals (e.g. 1234)
	hlescseq,     // for escape sequences
	hlerror,      // for parse errors, like a bad numeric or invalid escape
	hlident       // for other identifiers.  Probably won't use this.
};

// usually global variables are a bad thing, but for simplicity,
// we'll make an exception here.
// initialize our map with the keywords from our list:
map<string, short> hlmap = {
#include "res/keywords.txt"
};
// note: the above is not a very standard use of #include...

// map of highlighting spans:
map<int, string> hlspans = {
	{hlstatement, "<span class='statement'>"},
	{hlcomment, "<span class='comment'>"},
	{hlstrlit, "<span class='strlit'>"},
	{hlpreproc, "<span class='preproc'>"},
	{hltype, "<span class='type'>"},
	{hlnumeric, "<span class='numeric'>"},
	{hlescseq, "<span class='escseq'>"},
	{hlerror, "<span class='error'>"}
};
// note: initializing maps as above requires the -std=c++0x compiler flag,
// as well as #include<initializer_list>.  Very convenient though.
// to save some typing, store a variable for the end of these tags:
string spanend = "</span>";

string translateHTMLReserved(char c) {
	switch (c) {
		case '"':
			return "&quot;";
		case '\'':
			return "&apos;";
		case '&':
			return "&amp;";
		case '<':
			return "&lt;";
		case '>':
			return "&gt;";
		case '\t': // make tabs 4 spaces instead.
			return "&nbsp;&nbsp;&nbsp;&nbsp;";
		default:
			char s[2] = {c,0};
			return s;
	}
}

int isnum(string word)
{
	for(int i=0; i<word.length(); i++)
	{
		if (INSET(word[i],num)) return 1;
		else return 0;
	}
}
string cpp2html(string s)
{
	vector<string> wordList;
	string p_s = "";
	for(int i=0; i<s.length(); i++)
  {
		if(s[i] != ' ' && (isalpha(s[i]) || s[i] == '#'))
		 {
			 p_s = p_s + translateHTMLReserved(s[i]);
		 }
    else if(s[i] == ' ')
			{
				wordList.push_back(p_s);
				p_s = "";
				wordList.push_back(" ");
			}
		else
		{
			wordList.push_back(p_s);
			p_s = "";
			wordList.push_back(translateHTMLReserved(s[i]));
		}
  }
	wordList.push_back(p_s);
	for (int i=0; i<wordList.size(); i++){
	if(wordList[i].size()==0)
   {
     wordList.erase(wordList.begin()+i);
     i--;
   }
 }

 int state = 0;
 for (int i=0; i<wordList.size(); i++){
	//cout << "*" << wordList[i] << "*";
	map<string, short>::iterator it;
	it = hlmap.find(wordList[i]);

	if(wordList[i] == "/" && wordList[i+1] == "/")
	{
		state = hlcomment;
	}
	else if(wordList[i] == "\\")
	{
		state = hlescseq;
	}
	else if(isnum(wordList[i]))
	{
		state = hlnumeric;
	}

	if (state == 0 )
	{
		if(it != hlmap.end())
		{
			cout << hlspans[hlmap[wordList[i]]] + wordList[i] + spanend;
		}
		else
		{
			cout << wordList[i];
		}
	}
	else if (state == hlcomment)
	{
		if(wordList[i] == "/" && wordList[i+1] == "/")
			cout << hlspans[state] + wordList[i] + spanend;
		else
			cout << hlspans[state] + wordList[i] + spanend;
	}
	else if (state == hlnumeric)
	{
		cout << hlspans[state] + wordList[i] + spanend;
		if(wordList[i+1] == ";") state = 0;
		else if (!isnum(wordList[i+1])) state = hlerror;
	}
	else if (state == hlerror)
	{
		cout << hlspans[state] + wordList[i] + spanend;
	}
	else if (state = hlescseq)
	{
		if (INSET(wordList[i][0],escseq)){
			cout << hlspans[state] + wordList[i] + spanend;
			}
		else
		{
			state = hlerror;
			cout << hlspans[state] + wordList[i] + spanend;
		}
	}
}
	cout << endl;
}


int main() {
	string input;
	while(getline(cin,input)) {
	//	cout << " ";
		cpp2html(input);
	}
	return 0;
}
